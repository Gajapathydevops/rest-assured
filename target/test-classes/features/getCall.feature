@smoke1
Feature: Validation of get method

  Background: set up the base url
    Given set the base url

  Scenario Outline: Send a valid Request to get user details

    Given I send a request to the URL to get user details
    Then validate the responses <statusCode>
    Examples:
      | statusCode |
      | 200        |