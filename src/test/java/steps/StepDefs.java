package steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import org.json.JSONObject;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;


public class StepDefs {
    private ValidatableResponse validatableResponse;
    private String baseUrl = "http://dummy.restapiexample.com/";

    @Given("I send a request to the URL to get user details")
    public void sendRequest() {
        validatableResponse = given().contentType(ContentType.JSON)
                .when().get("api/v1/employee/1").then();

        System.out.println("Response :" + validatableResponse.extract().asPrettyString());
    }

    @Then("validate the responses {int}")
    public void validateTheResponsesStatusCode(int statusCode) {
        validatableResponse.assertThat().statusCode(statusCode);
    }

    @Given("set the base url")
    public void setTheBaseUrl() {
        RestAssured.baseURI = baseUrl;
    }


    @Given("Validate the responses using Junit Assert")
    public void validateTheResponsesUsingJunitAssert() {
        Response response = given()
                .contentType(ContentType.JSON)
                .when()
                .get("https://reqres.in/api/users?page=2");
        Assert.assertEquals(response.getStatusCode(), 200);
        Assert.assertEquals(response.jsonPath().get("data[2].first_name").toString(), "Tobias");
        JSONObject jo = new JSONObject(response.asString());
        for (int i = 0; i < jo.getJSONArray("data").length(); i++) {
            String fNames = jo.getJSONArray("data").getJSONObject(i).get("first_name").toString();
            System.out.println(fNames);
        }
    }

    @Given("Validate the XML responses using Junit Assert")
    public void validateTheXMLResponsesUsingJunitAssert() {

    }
}
