package steps;

import io.cucumber.java.en.Given;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matcher.*;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;


public class baseDefs {
    private static String baseUrl = "https://reqres.in/";


    @Given("^I send a request to the URL to get single user (.+) details$")
    public void getUser(String id) {
        given().baseUri(baseUrl)
                .when().get("api/users/" + id)
                .then()
                .statusCode(200)
                .log().all();
    }

    @Given("I send a request to the URL to create user details")
    public void createUser() {

        Map<String, Object> jsonAsMap = new HashMap<>();
        jsonAsMap.put("name", "gaja");
        jsonAsMap.put("job", "DL");
        given().baseUri(baseUrl)
                .contentType("application/json")
                .body(jsonAsMap)
                .when()
                .post("api/users")
                .then()
                .statusCode(201)
                .body("name", equalTo("6y6"))
                .log().all();
    }

    @Given("^I send a request to the URL to update user (.+) details$")
    public void updateUser(String id) {
        Map<String, Object> jsonAsMap = new HashMap<>();
        jsonAsMap.put("name", "hemas");
        jsonAsMap.put("job", "DL");
        given().baseUri(baseUrl)
                .when()
                .put("api/users/" + id)
                .then().statusCode(200)
                .log().all();
    }

    @Given("^I send a request to the URL to delete user (.+) details")
    public void deleteUser(String id) {
        given().baseUri(baseUrl)
                .when()
                .delete("api/users/" + id)
                .then()
                .statusCode(204)
                .log().all();
    }

    @Given("I send a request to the URL to create user details using POJO")
    public void createUserPOJO() {
        POJO data = new POJO();
        data.setJob("myjob");
        data.setName("my name");

        given()
                .baseUri(baseUrl)
                .contentType(ContentType.JSON)
                .body(data)
                .when()
                .post("api/users")
                .then()
                .body("name", equalTo("my name"))
                .log().all();
    }

    @Given("I send a request to the URL to create user details using JSON")
    public void createUserJSON() throws FileNotFoundException {
        File f = new File("./src/test/resources/data.json");
        FileReader fr = new FileReader(f);
        JSONTokener jt = new JSONTokener(fr);
        JSONObject data = new JSONObject(jt);

        given().baseUri(baseUrl)
                .contentType(ContentType.JSON)
                .body(data.toString())
                .when()
                .post("api/users")
                .then()
                .body("job", equalTo("devops"))
                .log().headers();
    }

    @Given("I send a request to the URL to get the cookies")
    public void getCookiesWeb() {
        Response response = given()
                .when()
                .get("https://www.google.com");
        Map<String, String> cookies = response.getCookies();
        System.out.println("cookies details" + response.getCookie("AEC"));
        for (String key : cookies.keySet()) {

            String val = response.getCookie(key);
            System.out.println(key + "            " + val);
            //System.out.println("Values" + val);
        }

    }

    @Given("I send a request to the URL to get the headers")
    public void getHeaders() {
        Response response = given()
                .when()
                .get("https://www.waitrose.com/");
        Headers hrs = response.getHeaders();
        for (Header hd : hrs) {
            System.out.println(hd.getName() + "        **********" + hd.getValue());
        }
    }
}
